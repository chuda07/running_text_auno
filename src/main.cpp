#include <Arduino.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  Serial.begin(9600);
}

void loop()
{
  lcd.setCursor(0,0);
  lcd.print("W Sistem Tertanam");
  lcd.setCursor(0,1);
  lcd.print("Polije - BWS");
  lcd.scrollDisplayRight();
  delay(300);
    
}
